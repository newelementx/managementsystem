package ro.managementsys.domain;

import java.util.Objects;

public class Item {
	private String category;
	private String name;
	private int quantity;
	private int price;
	private int maxQuantity;
	
	
	public Item(String itemCategory, String itemName, int quantity, int price, int maxQuantity) {
		this.category = itemCategory;
		this.name = itemName;
		this.price = price;
		this.maxQuantity = maxQuantity;
		this.quantity = quantity;
	}
	
		
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int balance) {
		this.quantity = balance;
	}	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getMaxQuantity() {
		return maxQuantity;
	}
	
	public void setMaxQuantity(int maxQuantity) {
		this.maxQuantity = maxQuantity;
	}		
	
	@Override
	public int hashCode() {
		return Objects.hash(category, maxQuantity, name, price, quantity);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Item other = (Item) obj;
		return Objects.equals(category, other.category) && maxQuantity == other.maxQuantity
				&& Objects.equals(name, other.name) && price == other.price && quantity == other.quantity;
	}


	@Override
	public String toString() {
		return "(category=" + this.category + ", name=" + this.name + ", currentQuantity=" +
				this.quantity + ", price=" + this.price + ", maxQuantity=" + this.maxQuantity + ")";
	}
	
}
