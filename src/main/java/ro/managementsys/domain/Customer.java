package ro.managementsys.domain;

public class Customer {
	private String username;
	private int balance;
	
	public Customer(String name, int balance) {
		this.username = name;
		this.balance = balance;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	public String toString() {
		return "username=" + this.username + ", balance=" + this.balance;
	}
}
