package ro.managementsys.ui;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import ro.managementsys.AppManager;
import ro.managementsys.logic.Store;

public class UserInterface {
	private Scanner scanner;
	private AppManager myManager;
	private Store myStore;
	private HashSet<String> categorySet = new HashSet<>();		// This Set contains all the distinct categories of products.
	

	public UserInterface(Scanner scanner, AppManager manager) {
		this.scanner = scanner;
		this.myManager = manager;
		this.myStore = this.myManager.getStore();		
	}

	public void start() {
		System.out.println("Type \"HELP\" to show available actions");
		
		while (true) {
			this.mantainCategories();			
			String input = scanner.nextLine().toLowerCase();

			if (input.equals("exit")) {
				break;
			}

			processCommand(input);
		}
	}

	public void processCommand(String input) {
		String[] pieces = input.split(" ");

		if (pieces[0].equals("print")) {
			
			// This part of code is used for checking the second string of input, intended for running of all product specific commands			
			if (pieces[1].equals("products") && pieces.length >= 3 && pieces.length <= 4) {

				// This if-statement is used for checking the input and for printing all the products of a specified category				
				if (pieces[2].equals("category") && pieces.length == 4) {					
					if (categorySet.contains(pieces[3])) {						// The products will be printed, if the specified category exists.		
						this.myStore.printProdByCategory(pieces[3]);
						Store.printInConsoleOrFile(pieces);						// The array will be parsed and printed in console of specified file.
					} else {
						Store.printInConsoleOrFile(pieces, "Unknown category inserted!");
					}
					
				// This if-statement is used for checking the input and printing all the products available in the Store object
				} else if (pieces[2].equals("all") && pieces.length == 3) {				
					this.myStore.printAllProducts();
					Store.printInConsoleOrFile(pieces);
					
				// This if-statement is used for checking the input for printing a specific product	
				} else if (checkProductExistance(pieces[2]) && pieces.length == 3) {				
						this.myStore.printTheProduct(pieces[2], pieces);  						
				} else {
					Store.printInConsoleOrFile(pieces, "Unknown product name inserted, or none was inserted!"); 
				}
				
			// This if-statement is used for checking the input and printing all the categories separated by a comma		
			} else if (pieces[1].equals("categories") && pieces.length == 2) {
				String formattedCategories = this.categorySet.stream().collect(Collectors.joining(","));  
				Store.printInConsoleOrFile(pieces, formattedCategories.toUpperCase());
			
			// This if-statement is used for checking the input and printing the current display mode (eg. Console or File_Path)
			} else if(pieces[1].equals("display_mode") && pieces.length == 2) {
				Store.printDisplayMode(pieces);
			} else {
				printErrorMessage(pieces);
			}			
			
		// This if-statement is used for checking the user input and for enabling him to buy a specific product based on a requested available quantity.	
		} else if (pieces[0].equals("buy") && pieces.length == 5 && pieces[3].equals("for")) {
			if (!checkProductExistance(pieces[1])) {
				Store.printInConsoleOrFile(pieces, "Unknown product \"" + pieces[1].toUpperCase() + "\" doesn't exists in the database of management system, please try again.");
				return;			
			} 
			
			try {
				int quantityToBuy = Integer.parseInt(pieces[2]);
				int currentQuantity = currentQuantityByProduct(pieces[1]);
				boolean userExists = checkIfClientExists(pieces[4]);

				if (!userExists) {
					Store.printInConsoleOrFile(pieces, "The user: " + pieces[4]	+ " doesn't exist in the database of management system, please try again.");
					return;										
				}
				
				if (quantityToBuy < 0) {
					Store.printInConsoleOrFile(pieces, "The command could not be executed, the user " + pieces[4] + " entered a negative quantity value.");						
					return;	
				} 
				
				if (quantityToBuy > currentQuantity) {
					Store.printInConsoleOrFile(pieces, "User " + pieces[4] + " cannot buy " + quantityToBuy + " " + pieces[1].toUpperCase()
							+ " because there is only " + currentQuantity + " " + pieces[1].toUpperCase() + " left.");
					return;
				} 
				this.myStore.buyProduct(pieces[1], quantityToBuy, currentQuantity, pieces[4], pieces);
				
			} catch (Exception ex) {
				Store.printInConsoleOrFile(pieces, "Wrong number format detected, please enter a positive integer(whole) number.");
			}
		
		// This if-statement is used for checking the input for replenishing the quantity of a product.				
		} else if (pieces[0].equals("replenish") && pieces.length == 3) {
			if (!checkProductExistance(pieces[1])) {
				Store.printInConsoleOrFile(pieces, "Unknown product \"" + pieces[1].toUpperCase() + "\" doesn't exists in the database of management system, please try again.");
				return;				
			} 
			
			int maxQuantityOfProduct = this.myStore.getStock().stream().filter(item -> item.getName().equals(pieces[1].toUpperCase()))
					.map(item -> item.getMaxQuantity()).reduce(0, (previous, quantity) -> previous + quantity);
			int itemCurrentQuantity = currentQuantityByProduct(pieces[1]);

			try {
				int quantityToReplenish = Integer.parseInt(pieces[2]);
				
				if (quantityToReplenish < 0) {
					Store.printInConsoleOrFile(pieces, "The command could not be executed, because you entered a negative value for quantity.");
					return;						
				}
				
				if (quantityToReplenish >= (maxQuantityOfProduct - itemCurrentQuantity)) {
					Store.printInConsoleOrFile(pieces, "You cannot replinsh this product: \"" + pieces[1].toUpperCase()	+ "\" because you exceed the maximum storage capacity by "
							+ (maxQuantityOfProduct - itemCurrentQuantity));
					return;						
				}				
				this.myStore.replenishThisProduct(pieces[1].toUpperCase(), quantityToReplenish, pieces);
				
			} catch (Exception ex) {
				Store.printInConsoleOrFile(pieces, "Wrong number format detected, please enter a positive integer(whole) number.");
			}
	
		// This part of code is used for checking the input intended for running of all ADD specific commands.
		} else if (pieces[0].equals("add") && pieces[1].equals("new") && pieces.length >= 4 && pieces.length <= 7) {
			
			// This if-statement is used for checking the input and for adding a new category for future products.
			if(pieces[2].equals("category") && pieces.length == 4) {
				if (!this.categorySet.contains(pieces[3])) {
					this.categorySet.add(pieces[3]);					
					Store.printInConsoleOrFile(pieces, "The new category \"" + pieces[3].toUpperCase() + "\" was successfully added in the store management system.");
				} else {
					Store.printInConsoleOrFile(pieces, "This category exists already, please try again to add a new one.");
				}
				
			// This if-statement is used for checking the input and for creating a new product, specifying the name, category, quantity and price for it.	
			} else if (pieces[2].equals("product") && pieces.length == 7) {
				if (checkProductExistance(pieces[3])) {
					Store.printInConsoleOrFile(pieces, "This product \"" + pieces[3].toUpperCase() + "\" already exists in the database of management system, try again with a new product.");
					return;						
				}
				
				if(!this.categorySet.contains(pieces[4])) {
					Store.printInConsoleOrFile(pieces, "The \"" + pieces[4].toUpperCase() + "\" category doesn't exist in our management system," 
							+ " you need first to add/create the category and then try again.");
					return;
				} 
				
				try {
					int quantityNewProduct = Integer.parseInt(pieces[5]);
					int priceNewProduct = Integer.parseInt(pieces[6]);
					
					if (quantityNewProduct > 0 && priceNewProduct > 0) {								
						this.myStore.addNewProduct(pieces[3].toUpperCase(), pieces[4].toUpperCase(), quantityNewProduct, priceNewProduct, pieces);
					} else {
						Store.printInConsoleOrFile(pieces, "The command could not be executed, because you entered a negative value for quantity or price.");
					}
				} catch (Exception ex) {
					Store.printInConsoleOrFile(pieces, "Wrong number format detected, please enter a positive integer(whole) number.");
				}
				
			// This if-statement is used for checking the input for adding a new client with a specific balance, to the store database.	
			} else if(pieces[2].equals("client") && pieces.length == 5) {
				if (checkIfClientExists(pieces[3])) {
					Store.printInConsoleOrFile(pieces, "The client already exists in store database, please try again with a new username");
					return;				
				} 
				
				try {
					int newClientBalance = Integer.parseInt(pieces[4]);
					if (newClientBalance > 0) {
						this.myStore.addClient(pieces[3], newClientBalance, pieces);
					} else {
						Store.printInConsoleOrFile(pieces, "The command could not be executed, because you entered a negative value for balance.");
					} 
				} catch (Exception e) {
					Store.printInConsoleOrFile(pieces, "Wrong number format detected, please enter a positive integer(whole) number.");
				}
				
			} else {
				printErrorMessage(pieces);
			}
		
		// This if-statement is used for checking the input for removing a specified product from the management system.	
		} else if(pieces[0].equals("remove") && pieces[1].equals("product") && pieces.length == 3) {	
			if (checkProductExistance(pieces[2])) {
				if(currentQuantityByProduct(pieces[2]) == 0) {		// Here we check the requested requirement that the product must have zero quantity.
					this.myStore.removeProduct(pieces[2], pieces);	// and then we remove it.
				} else {
					Store.printInConsoleOrFile(pieces, "Cannot remove " + pieces[2].toUpperCase() + " because quantity is not zero. Current quantity is " 
							+ currentQuantityByProduct(pieces[2]));
				}
			} else {
				Store.printInConsoleOrFile(pieces, "Unknown product \"" + pieces[2].toUpperCase() + "\" doesn't exists in the database of management system, please try again.");
			} 
		
		// This if-statement is used for changing the display_Mode, starting by default with CONSOLE and creating the ability to choose a file path.	
		} else if(pieces[0].equals("switch") && pieces[1].equals("display_mode") && pieces.length >= 3 && pieces.length <= 5) {
			
			// This if-statement checks if the user has chosen Console mode, and set the display mode accordingly.
			if(pieces[2].equals("console") && pieces.length == 3) {
				Store.setDisplayMode(false, null);
				Store.printInConsoleOrFile(pieces, "The display mode has been switched to the: " + pieces[2].toUpperCase());
			
			// This if-statement checks if the user has chosen File mode, and set the displayMode accordingly.
			} else if(pieces[2].equals("file") && pieces.length == 4) {
				File newFile = new File(pieces[3]);
				try {
					// creates a new, empty file if and only if a file with this name does not yet exist.
					newFile.createNewFile(); 			
					Store.setDisplayMode(true, pieces[3]);
					String goodOutcome = "The display mode has been switched to the " + pieces[3].toUpperCase();
					System.out.println(goodOutcome);
					Store.printInConsoleOrFile(pieces, goodOutcome);
				} catch (Exception e) {
					Store.printInConsoleOrFile(pieces, "An error happened. The program couldn't create/write to the file " + pieces[3]);
				}
				
			} else {
				printErrorMessage(pieces);
			}
			
		// This if-statement is used for checking the input and printing on screen the help message that contains all useful commands.
		} else if (pieces[0].equals("help") && pieces.length == 1) {
			printHelp(pieces);
			
		// This if-statement is used for printing on screen the help message that contains all useful commands.
		} else if (pieces[0].equals("export") && pieces.length == 2) {
			this.myManager.exportJSON(pieces[1]);
		} else {
			printErrorMessage(pieces);
		}
	}

	public void printHelp(String[] userCommand) {
		StringBuilder outputString = new StringBuilder();
		outputString.append("-PRINT PRODUCTS CATEGORY ${CATEGORY_NAME}"	+ "\n\t-> Se vor afisa produsele de la categoria respectiva.");
		outputString.append("\n-PRINT PRODUCTS ALL"
				+ "\n\t-> Se vor afisa toate produsele prezente in magazin, urmate de categorie, pret si cantitate.");
		outputString.append("\n-PRINT PRODUCTS ${PRODUCTS_NAME}"
				+ "\n\t-> Se va afisa produsul indicat, urmat de cantitata disponibila si pret.");
		outputString.append("\n-PRINT CATEGORIES" + "\n\t-> Se vor afisa toate categoriile prezente in magazin");
		outputString.append("\n-BUY ${PRODUCT} ${QUANTITY} FOR ${USERNAME}"
				+ "\n\t-> Se va cumpara un produs din magazin, pentru un utilizator, daca sold-ul si stoc-ul permite.");
		outputString.append("\n-REPLENISH ${PRODUCT} ${QUANTITY}"
				+ "\n\t-> Se va actualiza cantitatea produsului respectiv, in limita a cantitatii maxime presetata per produs.");
		outputString.append("\n-ADD NEW CATEGORY ${NAME}" + "\n\t-> Se va adauga o noua categorie pentru produse.");
		outputString.append("\n-ADD NEW PRODUCT ${NAME} ${CATEGORY} ${QUANTITY} ${PRICE}" + "\n\t-> Se va adauga un nou tip de produs.");
		outputString.append("\n-REMOVE PRODUCT ${NAME}"
				+ "\n\t-> Se va elimina produsul respectiv din lista de produse, doar cand cantitatea este 0.");
		outputString.append("\n-ADD NEW CLIENT ${USERNAME} {CLIENT_BALANCE}"
				+ "\n\t-> Se va adauga adauga un nou client, precizand username-ul si balanta acestuia.");
		outputString.append("\n-PRINT DISPLAY_MODE"
				+ "\n\t-> Se va indica modul selectat de afisare a rezultatelor, in CONSOLA ori in intr-un fisier extern.");
		outputString.append("\n-SWITCH DISPLAY_MODE CONSOLE sau FILE ${CALE_CATRE_FISIER}"
				+ "\n\t-> Se va selecta modul de afisare a rezultatelor comenzilor, in CONSOLA ori intr-un fisier (indicand calea).");
		outputString.append("\n-EXPORT ${FILE_NAME}"
				+ "\n\t-> Se va genera si exporta un fisier JSON ce va contine toate informatiile curente din sistem. Fisierul este creat in C:\\output\\");
		outputString.append("\n-EXIT" + "\t-> Aceasta comanda va permite sa opriti executia programului de gestiune.");

		Store.printInConsoleOrFile(userCommand, outputString.toString());
	}

	// This method is used to maintain a distinct set of categories, parsing all the existing products in the management system.
	public void mantainCategories() {
		myStore.getStock().stream().forEach(t -> this.categorySet.add(t.getCategory().toLowerCase()));
	}

	// This method is used for checking if and requested product exists in the item list, for easier access.
	public boolean checkProductExistance(String productName) {
		List<String> itemNamesList = this.myStore.getStock().stream().map(t -> t.getName().toLowerCase()).collect(Collectors.toList());
		if (itemNamesList.contains(productName)) {
			return true;
		}

		return false;
	}

	// This method is returning the current quantity of a product.
	public int currentQuantityByProduct(String productName) {
		int productCurrentQuantity = this.myStore.getStock().stream().filter(item -> item.getName().equals(productName.toUpperCase())).map(item -> item.getQuantity())
				.reduce(0, (previous, quantity) -> previous + quantity);
		return productCurrentQuantity;
	}

	// This method checks if a specified customer exists in the management system database.
	public boolean checkIfClientExists(String clientName) {
		boolean heExists = this.myStore.getClients().stream().map(client -> client.getUsername()).anyMatch(name -> name.equals(clientName));
		if (heExists) {
			return true;
		}
		return false;
	}
	
	// This method is used for printing general errors message.
	public void printErrorMessage(String[] userCommand) {
		Store.printInConsoleOrFile(userCommand, "Unknown command, please try again.");
	}
}
