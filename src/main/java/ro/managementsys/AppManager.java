package ro.managementsys;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ro.managementsys.logic.Store;
import ro.managementsys.ui.UserInterface;

public class AppManager {	
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();  // This creates a Gson object for later use
	private Store store;

	public static void main(String[] args) {		
		AppManager manager = new AppManager();
		
		// We we check from beginning that user not forgot to import the JSON file.
		if(args.length == 0) {				
			System.out.println("You forgot to import the JSON file, needed for initialization(e.g C:\\input.json)");
			return;
		}
		manager.importJSON(args[0]);
		
		Scanner scanner = new Scanner(System.in);
		new UserInterface(scanner, manager).start();
	}
	
	
	// This method is used for system initialization with the current informations, by importing a JSON file.
	public  void importJSON(String filePath) {			
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath));  // We use a bufferedReader to be more efficient.
			
			// This converts the JSON file to Java object, and restore it as type Store.
			store = gson.fromJson(reader, Store.class);							
			reader.close();
			System.out.println("Import succeded");						
			
		} catch (IOException e) {
			System.out.println("The file couldn't be found, please try again");			
		}
	}
	
	
	// This method is used for exporting the current state of Store object, as a JSON file
	public void exportJSON(String fileName) {		
		try {			
			FileWriter writer = new FileWriter(fileName);
			
			// This converts the Java Object to JSON file.
			gson.toJson(store, writer);							
			writer.close();
			System.out.println("Exported with success");
			
		} catch (IOException e) {
			System.out.println("The file couldn't be exported, please try again");			
		}		
	}	
	
	// This method is used for enter in possession of a private reference of Store object
		public Store getStore() {
			return this.store;
		}
}

